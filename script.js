let inputNumber = Number(prompt('Enter a number:'));
console.log(`The number you provided is ${inputNumber}`);

for (i = inputNumber; i >= 0; i--) {

    if (i <= 50) {
        console.log(`The current value is ${i}. Terminating the loop.`)
        break;
    }

    if (i % 10 === 0) {
       console.log('The number is divisible by 10. Skipping the number.');
       continue;
    }

    if (i % 5 === 0) {
        console.log(i);
    }

}

// 

let string = 'supercalifragilisticexpialidocious';
let consonants = '';
let vowels = 'aeiou';

for (i = 0; i < string.length; i++) {

    if (vowels.includes(string[i])) {
        continue;
    }

    else {
        consonants += string[i];
    }

}

console.log(string);
console.log(consonants);